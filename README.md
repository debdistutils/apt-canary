# apt-canary

## What is apt-canary?

Apt-canary allows [apt](https://salsa.debian.org/apt-team/apt) to
confirm that the archive release files have been seen (and possibly
verified in various ways) by an auditable third party.

Apt-canary requires
[apt-verify](https://gitlab.com/debdistutils/apt-verify).

## Overview

Apt-based distributions uses the `apt` or `apt-get` tools to install
and upgrade packages.  The process to make sure unintended packages
are not downloaded is based on an OpenPGP signature on the `Release`
or `InRelease` files.  The signature is verified by
[GnuPG](https://www.gnupg.org/) using the
[gpgv](https://www.gnupg.org/documentation/manuals/gnupg/gpgv.html)
tool with a keyring of trusted OpenPGP keys stored in
`/etc/apt/trusted.gpg.d/`.

If someone uses this OpenPGP private key for malicious purposes they
can get your system to upgrade and install packages whenever your
system installs or upgrade packages.

The purpose of `apt-canary` is to enable auditing when that happens,
to help identify the malicious actor and to understand what happened.
It does this by preventing using a release file when that file has not
already been seen and validated by a third party.

Release files are verified by the `apt-canary-gpgv` script (before
`apt` calls out to `gpgv`) through the retrieval and verification of
so called "apt canary witness" files.  These witness files are served
on a HTTPS site and contain information about the release file.  A
cryptographic checksum (currently SHA256) is derived from the release
file under verification, to yield a URL where the witness file can be
found.  The witness file is validated by verifying that it knows its
own URL which include the checksum.

One goal is to achieve global transparency for the release files that
machines download and rely on.  This idea is inspired by [Certificate
Transparency](https://en.wikipedia.org/wiki/Certificate_Transparency).
This reduces the amount of damage someone who steals an OpenPGP
archive signing key can do to do things that can be publicly audited
afterwards.

## Installation and dependencies

You need to setup
[apt-verify](https://gitlab.com/debdistutils/apt-verify) first.

Then put the `apt-canary-gpgv` file in `/etc/apt/verify.d`.

The script works with any POSIX-compliant /bin/sh such as dash or GNU
bash.

The script uses `wget` (`apt-get install wget`) to download witness
files, `sha256sum` (`apt-get install coreutils`) to compute SHA256
checksum on release files, and `logger` (`apt-get install bsdutils`)
for sending messages to the system log file.  Other common tools like
`cat`, `test`, `sed` and `grep` are required.

Naturally, `apt` needs to be installed and the script uses the
`apt-config` tool to discover the witness URL.

## Usage

Configure the URL to use for verification.  For example, run the
following as root:

```
echo 'Canary::Base-URL "https://gitlab.com/debdistutils/canary/'$(lsb_release --short --id | tr A-Z a-z)'/-/raw/main/canary";' > /etc/apt/apt.conf.d/76canary
```

On a Trisquel system, it will result in the
`/etc/apt/apt.conf.d/76canary` file containing:

```
Canary::Base-URL "https://gitlab.com/debdistutils/canary/trisquel/-/raw/main/canary";
```

Replace the string `trisquel` with `pureos`, `debian`, `ubuntu` or
`devuan` as appropraite, to use each of the following canary sites:

- Trisquel: https://gitlab.com/debdistutils/canary/trisquel
- PureOS: https://gitlab.com/debdistutils/canary/pureos
- Gnuinos: https://gitlab.com/debdistutils/canary/gnuinos
- Ubuntu: https://gitlab.com/debdistutils/canary/ubuntu
- Debian: https://gitlab.com/debdistutils/canary/debian
- Devuan: https://gitlab.com/debdistutils/canary/devuan

These git repositories are automatically updated through GitLab CI/CD
via https://gitlab.com/debdistutils/debdistget/ and you may setup your
own site if you don't want to rely on this service.

Test it by running `apt-get update` and you should see something like
the following in `/var/log/syslog`:

```
Feb  1 17:40:27 kaka apt-canary: successful witness https://gitlab.com/debdistutils/canary/trisquel/-/raw/main/canary/da99b64cc8768b93527a267f77121f7bcd09bbf284d0053b45d859a71f888e67.witness
```

On failures you will see the following line:

```
Feb  1 17:40:03 kaka apt-canary: unable to find successful witness https://gitlab.com/debdistutils/canary/trisquel/-/raw/main/canary/ea99b64cc8768b93527a267f77121f7bcd09bbf284d0053b45d859a71f888e67.witness
```

Followed by a dump of the complete release file data and signature
that was downloaded by apt.  The purpose of logging these details is
to have an audit log of what data a potential malicious actor sent to
your machine, for further examination.

```
Feb  1 17:46:28 kaka apt-canary-datafile: Origin: Trisquel
Feb  1 17:46:28 kaka apt-canary-datafile: Label: Trisquel
Feb  1 17:46:28 kaka apt-canary-datafile: Suite: aramo-security
...
Feb  1 17:46:28 kaka apt-canary-signfile: -----BEGIN PGP SIGNATURE-----
Feb  1 17:46:28 kaka apt-canary-signfile: 
Feb  1 17:46:28 kaka apt-canary-signfile: iQIzBAEBCgAdFiEEYDZMmGn5JFBCHwwisTjKRQwFES8FAmPZZv0ACgkQsTjKRQwF
Feb  1 17:46:28 kaka apt-canary-signfile: ES9esA//czoomzyH7/Dc6P7jfjAjgl3P35B2tgt7FOWqcgNyilOkQ/7rMibQ59VE
...
```

## Background

Internally apt roughly behave like this:

1. Download a number of InRelease (or Release/Release.gpg) files from some archive mirror.  For example:
```
wget -q http://archive.trisquel.org/trisquel/dists/aramo/InRelease
```

2. Verify the release files using `apt-key verify` which in turn uses GnuPG's `gpgv`.  For example (download [trisquel-archive-keyring.gpg](https://gitlab.trisquel.org/trisquel/trisquel-packages/-/raw/master/extra/trisquel-keyring/keyrings/trisquel-archive-keyring.gpg) if you don't have it):
```
gpgv --keyring /etc/apt/trusted.gpg.d/trisquel-archive-keyring.gpg InRelease
```

3. Successfully verified files are moved to a locally trusted store,
   and package installation or upgrades based on its content.

Understanding how it works, we can identify a couple of concerns:

- Control of the trust keyring itself, or any of the OpenPGP private
  keys in it, is sufficient to cause your system to install or upgrade
  potentially malicious packages.

- The release files are fairly small and not part of some larger
  integrity protected blob.

- Often unauthenticated HTTP is used that allows anyone on the path to
  your machine to replace content with something else.

- Serving different content to different machines is possible by
  filtering on IP-address.

- The server may serve you stale data, possibly on a per-IP basis, for
  some time to slow down your systems from upgrading packages.

Some may argue that using HTTPS would solve the problems.  It would
address the problem of someone in the network replacing the release
files that your machine downloads.  However HTTPS would not prevent
anyone who control the OpenPGP key to publish malicious data for
everyone, or just for a subset of IP addresses on some subset of
archive mirrors.  HTTPS would also not address a archive mirror from
serving stale data on a per-IP basis for some time.

## Design

Apt-canary is ran by apt-verify before gpgv to do its work.

The idea is that for each release file that apt attempts to verify
using `gpgv`, the `apt-verify-gpgv` tool will invoke gpgv, but will
first also call `apt-canary-gpgv` to retrieve and verify the canary
witness file for it.

The URL to retrieve is the Canary::Base-URL (specified in a
/etc/apt/apt.conf.d/ file, see below) and the content is expected to
be in the apt canary witness file format.

The witness files needs to contain a line like this:

```
Canary: https://gitlab.com/debdistutils/canary/trisquel/-/raw/main/canary/fad7cacac6f9a8b3a47af414ea335e5dae36beb5a34a58bc9afe1a5ababa7b0b.witness
```

Human review of the apt canary sites results in the strongest
protection, however even an automatically updated apt canary site
provides protection against per-ip substitution attacks and allows
auditing of OpenPGP key misuse that would not otherwise be detected or
auditable.

## License

All files in the apt-canary project are published under the AGPLv3+
see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Status

I'm currently using apt-canary on my laptop which is running Trisquel
aramo, on a Dell R630 amd64 server running Trisquel nabia, on a Talos
II ppc64el machine running Debian bullseye, and several amd64 virtual
machines running Debian bullseye.

## Related work

Some related efforts:

- https://gitlab.com/debdistutils/apt-verify
- https://gitlab.com/debdistutils/debdistget
- https://github.com/sigstore/rekor
- https://www.sigsum.org/
- https://arxiv.org/abs/1711.07278
- https://github.com/FreeBSDFoundation/binary-transparency-notes
- https://debconf17.debconf.org/talks/153/
- https://wiki.debian.org/Derivatives/Integration#Patches

## Further work

This is a proof-of-concept project that does something useful in some
environments, but the following are known areas for further work.

- If the canary URL you use is not updated, your machine won't update
  anymore as it will reject all newer release files.  While this is
  intentional, perhaps there could be a downgrade mechanism or
  notification method for this?  What does apt do when it notices a
  release files with a Valid-Until in the past?  Maybe a similar
  mechanism could be used.

- If the canary/-files are not generated based on ALL versions of
  dists/ files there is a risk that some system will get a
  non-canary'ed file from a archive mirror.  The solution is to pull
  servers more often when pulling in dists/-files and generating
  canary/-files.

- Human auditing of the release files stored in git repositories is
  not trivial.

- Currently syslog is used for outputing debug information, which
  would be nicer to print where `apt-get update` is run on.  While
  sending it to /dev/tty may work it seems fragile.  Logging unknown
  datafile/signfile's to syslog for auditing still seems appropriate
  though.

- If the same release file exists on more than one server that is
  being monitored (e.g., Ubuntu archive.ubuntu.com vs
  ports.ubuntu.com) for different architectures, things fail.

- The witness file format is not fully fleshed out.  Should it support
  some other hash value like a SHA3 hash to not have SHA256 be the
  single point of failures?  Is the mechanism to validate it a good
  idea?  The files could be OpenPGP clearsigned to avoid using HTTPS
  as a trust mechanism.

- Replacing `gpgv` is a a hack.  The functionality could be integrated
  into apt properly, or via some other existing extension mechanism.

- Denial of Service considerations.  Fetching a witness file from a
  site opens up for that site owner to send malicious data, for
  example a 5GB file.  The shell script does not parse the file,
  currently, but a bug in wget could cause problems.  
  Perhaps an additional `| head -c1024 |` could be added to reduce the
  amount of data that is being grep'ed in.  This is not conceptually
  different than someone injecting malicious data into apt when it
  fetches the release file in the first place though.

- Setup different kind of witness publishers.  The simplest we have
  right now (debdistget) is one that creates a witness for a release
  file it has seen.  We could also setup a publisher that before
  creating the witness, it downloads any modified packages and store
  them for further auditing as well.  We could also have a publisher
  that attempts to build the package, and would only create a witness
  on successful builds.  One step further, another publisher could
  only create witnesses is the build was reproducible.  Another idea
  is for a publisher to merely build the package and publish
  diffoscope output and a debdiff on the source packages compared to
  the previous version, and wait for manualy review by a human to
  create the witness manually or through an automated merge request
  that has to be approved and merged to the main branch.
